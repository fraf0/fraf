#!/usr/bin/python
# -*- coding: utf-8 -*-
# Licence : GPLv3
#
# Created by fraf (hack_AnUsualEmailSeparator_fraf_point_fr)
# from http://stackoverflow.com/questions/37140846/how-to-convert-ipv6-link-local-address-to-mac-address-in-python
#
# History
#   20161205 : Creation
#

import argparse

def mac2ipv6(mac):
    # only accept MACs separated by a colon
    parts = mac.split(":")

    # modify parts to match IPv6 value
    parts.insert(3, "ff")
    parts.insert(4, "fe")
    parts[0] = "%x" % (int(parts[0], 16) ^ 2)

    # format output
    ipv6Parts = []
    for i in range(0, len(parts), 2):
        ipv6Parts.append("".join(parts[i:i+2]))
    ipv6 = "fe80::%s/64" % (":".join(ipv6Parts))
    return ipv6

def ipv62mac(ipv6):
    # remove subnet info if given
    subnetIndex = ipv6.find("/")
    if subnetIndex != -1:
        ipv6 = ipv6[:subnetIndex]

    ipv6Parts = ipv6.split(":")
    macParts = []
    for ipv6Part in ipv6Parts[-4:]:
        while len(ipv6Part) < 4:
            ipv6Part = "0" + ipv6Part
        macParts.append(ipv6Part[:2])
        macParts.append(ipv6Part[-2:])

    # modify parts to match MAC value
    macParts[0] = "%02x" % (int(macParts[0], 16) ^ 2)
    del macParts[4]
    del macParts[3]

    return ":".join(macParts)


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--frommac', help='Print the SLAAC IPv6 from MAC addrress',default=False)
    parser.add_argument('--fromipv6', help='Print the MAC address from SLAAC IPv6',default=False)
    parser.add_argument('--debug', help='Print more information', default=False)
    args = parser.parse_args()

    mac = args.frommac
    ipv6 = args.fromipv6
    debug=args.debug

    if mac:
        print mac2ipv6(mac)
    elif ipv6:
            print ipv62mac(ipv6)
    else:
        print "Error, missing mac or ipv6."
