#!/bin/bash

while read func; do unset -f ${func}; done < <(declare -F | awk '{print $NF}')
shopt -u expand_aliases
PATH=/usr/local/bin:/usr/bin:/bin:/usr/sbin

[ -z "${Debug}" ] || set -x

vgDir="/usr/local/pentest/SecLists/Passwords/ /usr/local/pentest/fraf/wordlists/"
vgExcludedWords="withcount"

for vlDir in ${vgDir}
  do
    for vlWd in $(ls ${vlDir}/*.txt)
      do
        echo -e "\n${vlWd}\n"
        john --wordlist=${vlWd} ${*} 2>/dev/null
      done
  	#for vlWd in $(ls ${vlDir}/*.gz | grep -v ${vgExcludedWords})
  	#  do
  	#    cd /tmp/ && vlWdName=$(gzip -dc ${vlWd} | tar xvf -) && cd -
    	#    echo -e "\n${vlWd}\n"
  	#    john --wordlist=/tmp/${vlWdName} ${*}
	#    rm /tmp/${vlWdName}
  	#  done
  done
