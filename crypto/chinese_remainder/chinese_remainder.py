#!/usr/bin/python
# -*- coding: utf8 -*-
#
# Licence : GPLv3
#
# Created by fraf (hack_AnUsualEmailSeparator_fraf_point_fr)
# from http://www.pwntester.com/blog/2014/01/17/hackyou2014-crypto400-write-up/
#
# Implemente the Rabin crypto system
# "https://fr.wikipedia.org/wiki/Cryptosyst%C3%A8me_de_Rabin"
# 
# History
#   20170409 : Add base argument
#              Change the hexa padding way. 
#   20160824 : Creation
#
# Test with nr.lst and exponent=3

import argparse

from struct import *  
from operator import mod

def eea(a,b):  
    """Extended Euclidean Algorithm for GCD"""
    v1 = [a,1,0]
    v2 = [b,0,1]
    while v2[0]<>0:
       p = v1[0]//v2[0] # floor division
       v2, v1 = map(lambda x, y: x-y,v1,[p*vi for vi in v2]), v2
    return v1

def inverse(m,k):  
     """
     Return b such that b*m mod k = 1, or 0 if no solution
     """
     v = eea(m,k)
     return (v[0]==1)*(v[1] % k)

def crt(ml,al):  
     """
     Chinese Remainder Theorem:
     ms = list of pairwise relatively prime integers
     as = remainders when x is divided by ms
     (ai is 'each in as', mi 'each in ms')

     The solution for x modulo M (M = product of ms) will be:
     x = a1*M1*y1 + a2*M2*y2 + ... + ar*Mr*yr (mod M),
     where Mi = M/mi and yi = (Mi)^-1 (mod mi) for 1 <= i <= r.
     """

     M  = reduce(lambda x, y: x*y,ml)        # multiply ml together
     Ms = [M/mi for mi in ml]   # list of all M/mi
     ys = [inverse(Mi, mi) for Mi,mi in zip(Ms,ml)] # uses inverse,eea
     return reduce(lambda x, y: x+y,[ai*Mi*yi for ai,Mi,yi in zip(al,Ms,ys)]) % M

def root(x,n):  
    """Finds the integer component of the n'th root of x,
    an integer such that y ** n <= x < (y + 1) ** n.
    """
    high = 1
    while high ** n < x:
        high *= 2
    low = high/2
    while low < high:
        mid = (low + high) // 2
        if low < mid and mid**n < x:
            low = mid
        elif high > mid and mid**n > x:
            high = mid
        else:
            return mid
    return mid + 1

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--file', help='File containing n (modulus),r (remainder) with commas separator', required=True)
    parser.add_argument('--base', help='The base of input number (16 for hexa, ...)', default=10, required=False)
    parser.add_argument('--exponent', help='The public exponent', required=True)
    parser.add_argument('--debug', help='Print more information', default=False)
    args = parser.parse_args()

    file = args.file
    base = int(args.base)
    exponent = int(args.exponent)
    debug=args.debug

    n_list = []
    r_list = []

    with open(file,'r') as f:
        data=f.readlines()
        for linenb in range(len(data)):
            n, r = data[linenb].split(",")
            n_list.append(int(n,base))
            r_list.append(int(r,base))

    F = crt(n_list,r_list)
    intflag = root(F,exponent)
    flaghex = hex(intflag)[2:-1]

    # Add a leading zero if necessary
    flaghex = '0' * (len(flaghex) % 2) + flaghex

    flag = flaghex.decode('hex')
    print flag
