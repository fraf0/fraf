#!/usr/bin/python
# -*- coding: utf8 -*-
#
# Licence : GPLv3
#
# Created by fraf (hack_AnUsualEmailSeparator_fraf_point_fr)
# from https://neg9.org/news/2015/8/12/openctf-2015-veritable-buzz-1-crypto-300-writeup
#
# History
#   20160815 : Creation
#
# sha256 only !
#

from ecdsa import VerifyingKey, SigningKey, NIST384p
from ecdsa.numbertheory import inverse_mod
from hashlib import sha256
import argparse


def verify(m,s,pub):
	print "\nVerifing \"" + m + "\" and his sig:", pub.verify(s, m, hashfunc=sha256)


def crack(msg1,sig1,msg2,sig2,pub_k):
	
	curve = pub_k.curve

	n = pub_k.curve.order
	if debug: print "\nn:", n

	# This to cut the sig in r and s
	# For a NIST384p : 384 / 8  =48
	sub_part_length = pub_k.curve.baselen
	if debug: print "baselen:",sub_part_length

	r1 = sig1[:sub_part_length]
	s1 = sig1[sub_part_length:]

	r2 = sig2[:sub_part_length]
	s2 = sig2[sub_part_length:]

	if debug:
		print "\ns1:", s1.encode('hex')
		print "s2:", s2.encode('hex')
		print "r: ",r1.encode('hex')
		print "Identical r:", r1==r2

	m1hash=sha256()
	m1hash.update(msg1)

	m2hash=sha256()
	m2hash.update(msg2)
	
	# Convert s1, s2, r1, r2 , z1 and z2 to int

	s1 = int(s1.encode('hex'), 16)
	s2 = int(s2.encode('hex'), 16)
	r1 = int(r1.encode('hex'), 16)
	r2 = int(r2.encode('hex'), 16)
	z1 = int(m1hash.hexdigest(),16)
	z2 = int(m2hash.hexdigest(),16)

	if debug: 
		print "\ns1 int:", s1
		print "s2 int:", s2

		print "\nr1 int:", r1

		print "\nz1 int", z1
		print "z2 int", z2

	# And now, lets crack !!!

	sdiff_inv = inverse_mod(((s1 - s2) % n), n)
	k = ( ((z1 - z2) %n) * sdiff_inv) % n
	r_inv = inverse_mod(r1, n)
	da = (((((s1 * k) % n) - z1) % n) * r_inv) % n
	
	if debug: print "\nRecovered Da: " + hex(da)

	# We can now return the private key
	return SigningKey.from_secret_exponent(da, curve=curve)


if __name__ == '__main__':

	parser = argparse.ArgumentParser()
	parser.add_argument('--msg1', help='The 1st signed message', required=True)
	parser.add_argument('--sig1', help='The 1st message\'s signature in hex', required=True)
	parser.add_argument('--msg2', help='The 2nd signed message', required=True)
	parser.add_argument('--sig2', help='The 2nd message\'s signature in hex', required=True)
	parser.add_argument('--pubkeyfile', help='The public key\'s file, in PEM format', required=True)
	parser.add_argument('--debug', help='Print more information', default=False)
	args = parser.parse_args()

	msg1 = args.msg1
	sig1 = args.sig1.decode('hex')
	msg2 = args.msg2
	sig2 = args.sig2.decode('hex')
	pubkeyfile = args.pubkeyfile
	debug=args.debug

	pub_k = VerifyingKey.from_pem(open(pubkeyfile).read())

	print "\n === Verifying === "
	# Verifying the signature
	verify(msg1, sig1, pub_k)
	verify(msg2, sig2, pub_k)

	print "\n === recover === "
	# Recover the private key with only the msg, the sign and the public key 
	# (in fact, we need n -the order- and the key bits length -386 for a NIST384p-)
	recovered_priv_k = crack(msg1,sig1,msg2,sig2,pub_k)

	print recovered_priv_k.to_pem()
