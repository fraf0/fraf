#!/usr/bin/python
# -*- coding: utf8 -*-
#
# Licence : GPLv3
#
# Created by fraf (hack_AnUsualEmailSeparator_fraf_point_fr)
#
# Implemente the Rabin crypto system
# "https://fr.wikipedia.org/wiki/Cryptosyst%C3%A8me_de_Rabin"
# 
# History
#   20160824 : Creation
#
# Example
# $ ./Rabin.py --p 7 --q 11 --clear 0x14
# 15
# $ ./Rabin.py --p 7 --q 11 --clear $(echo -ne '\x14')
# 15
# $ ./Rabin.py --p 7 --q 11 --cipher 15
# [64, 13, 20, 57]
# $ ./Rabin.py --p 7 --q 11 --cipher 0xf
# [64, 13, 20, 57]
# $ 

import argparse


def xgcd(x, y):
	"""Extended Euclidean Algorithm"""
	s1, s0 = 0, 1
	t1, t0 = 1, 0
	while y:
		q = x // y
		x, y = y, x % y
		s1, s0 = s0 - q * s1, s1
		t1, t0 = t0 - q * t1, t1
	return x, s0, t0  


def Rabin_decrypt(p,q,cipher):
	"""
	Take
		p, q as int
		cipher as int
	Return an array of integer
	"""
	n = p * q
	mp=pow(cipher,((p+1)/4),p)
	mq=pow(cipher,((q+1)/4),q)
	if debug: print "mp:", mp, "mq:",mq

	a, yp, yq = xgcd(p, q)
	if debug: print a, yp, yq
	if debug: print "Verif:", yp*p+yq*q

	r = (yp*p*mq + yq*q*mp) % n
	_r = n - r
	s = (yp*p*mq - yq*q*mp) % n
	_s = n -s
	return [r, _r, s, _s]


def Rabin_crypt(p, q, clear):
	"""
	Take
		p, q as int
		clear as str
	Return an integer.
	"""
	n = p * q
	return (int(clear.encode('hex'),16)**2) % n


if __name__ == "__main__":

	clear = None
	cipher = None

	parser = argparse.ArgumentParser(description='Rabin crypto system')
	parser.add_argument('--p', help='The p integer', required=True)
	parser.add_argument('--q', help='The q integer', required=True)
	group = parser.add_mutually_exclusive_group(required=True)
	group.add_argument('--clear', help='The clear text to encrypt')
	group.add_argument('--cipher', help='The cipher text to decrypt')
	parser.add_argument('--debug', help='Print more information', action='store_true', default=False)
	parser.add_argument('--moreout', help='Print decrypt result in hex and text mode', action='store_true', default=False)
	args = parser.parse_args()

	p = int(args.p)
	q = int(args.q)
	if args.clear != None: clear = args.clear
	if args.cipher != None: cipher = args.cipher
	moreout=args.moreout
	debug=args.debug

	# Convert clear/cipher
	if args.clear != None:
		if args.clear[0:2] == '0x':
			clear = args.clear[2:].strip('L').decode('hex')
		else:
			clear = args.clear
	if args.cipher != None:
		if args.cipher[0:2] == '0x':
			cipher = int(args.cipher[2:].strip('L'),16)
		else:
			cipher = int(args.cipher)

	if clear != None:
		print Rabin_crypt(p, q, clear)
	if cipher != None:
		result = Rabin_decrypt(p, q, cipher)
		print result
		if moreout:
			for i in range(len(result)):
				print 'For result', i+1, ':', result[i]
				hexresult = hex(result[i])
				if len(hexresult) % 2 == 1: hexresult = '0x0'+hexresult[2:]
				print '\t',hexresult
				print '\t',hexresult[2:].strip('L').decode('hex')
