#!/usr/bin/python
# -*- coding: utf-8 -*-
# Licence : GPLv3
# Created by fraf (hack_AnUsualEmailSeparator_fraf_point_fr)
# From http://www.shannonentropy.netmark.pl/ and https://fr.wikipedia.org/wiki/Entropie_de_Shannon

# Implemente the Shannon algorithm
# 
# History
#   20170327 : Creation

from base64 import b64decode
from math import log
import argparse

class ShannonEntropy(object):
    def __init__(self, input):
        self.alphabet = dict()
        self.input = input
        self.line_length = len(self.input[0])
        self.input_length = len(self.input)
        for inc in range(len(self.input)):
            if len(self.input[inc]) != self.line_length:
                raise Exception("All input member must have the same length.")

    def Entropy(self, level=None):
        if level != None:
            new_input = [ [] ]
            for inc in range(len(self.input)):
                new_input[0].append(self.input[inc][level])
            s = ShannonEntropy(new_input)
            return s.Entropy()

        for inc in range(len(self.input)):
            for xcar in range(len(self.input[inc])):
                car = self.input[inc][xcar].decode("hex")
                if self.alphabet.has_key(car):
                    self.alphabet[car] += 1
                else:
                    self.alphabet[car] = 1

        self.global_probability = 0.0

        for key in self.alphabet.keys():
            count = self.alphabet[key]
            probability = float(count) / ( self.line_length * self.input_length)
            self.alphabet[key] = [count, probability]
            self.global_probability = self.global_probability + ( probability * log(probability, 2))

        return self.global_probability, self.alphabet

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('--tokens', help='The file tokens file', required=True)
    parser.add_argument('--levels', help='The levels(index) to analyse, separated by a comma', required=False)
    parser.add_argument('--all', help='Generate all indexes', action='store_true', default=False)
    parser.add_argument('--base64', help='Treated input as base64', action='store_true', default=False)
    parser.add_argument('--debug', help='Print extra debug information', action='store_true', default=False)
    args = parser.parse_args()

    debug=False
    if args.debug:
        debug=True

    if args.tokens:
        tokens_list =[]
        f = open(args.tokens,'r')
        tokens = f.readlines()
        f.close()

        for token in tokens:
            if args.base64:
                token= b64decode(token)
                tokens_list.append([char.encode("hex") for char in token])

            tokens_list.append([char.encode("hex") for char in token.strip().decode("hex")])


    if debug:
        for token in tokens_list:
            print token

    s = ShannonEntropy(tokens_list)
    data = s.Entropy()

    print "Global Shannon entropy: {}".format(data[0])
    print "Character, Occurence, Probability"
    for k,v in data[1].iteritems():
        print "0x" + k.encode("hex"), v[0], v[1]

    if args.levels:
        for idx in args.levels.split(','):
            data = s.Entropy(int(idx))
            print "level:{} : {}".format(str(idx), data)

    if args.all:
        for idx in range(len(tokens_list[0])):
            data = s.Entropy(idx)
            print "level:{} : {}".format(str(idx), data)
            